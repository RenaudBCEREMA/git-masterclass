class Complexe():
	"""
    Une classe qui gère les nombres complexes de manière élémentaire

    ...

    Attributes
    ----------
    PR : float
        la partie réelle du complexe
    PI : float
        la partie imaginaire du complexe
   
	Methods
    -------
    afficheComplexe()
        Affiche le complexe
	ajouter()
		Ajoute un complexe
    """

	def __init__(self, re : float, im : float) :
		self.PR = re
		self.PI = im
	
	def afficheComplexe(self):
		"""Affiche le complexe
		"""
		if self.PI < 0 :
			print(f'Le complexe est {self.PR} {self.PI}i')
		else :
			print(f'Le complexe est {self.PR} + {self.PI}i')

	def ajouter(self,c):
		"""Ajoute un complexe

		Parameters
		----------
		c : Complexe à ajouter
		"""
		self.PR += c.PR
		self.PI += c.PI

	def soustraire(self,c):
		"""
		
		"""
		self.PR -= c.PR
		self.PI-= c.PI
