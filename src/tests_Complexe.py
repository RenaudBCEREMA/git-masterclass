from .Complexe import Complexe

def test_afficheComplexe(capsys):
    """Test de la méthode affiche complexe
    """
    c1 = Complexe(-1,3)
    c1.afficheComplexe()
    captured = capsys.readouterr()
    assert (captured.out) == 'Le complexe est -1 + 3i\n'
    
def test_ajouter():
   c1 = Complexe(-1,3)
   c2 = Complexe(2,7)
   c1.ajouter(c2) 
   assert(c1.PR) == 1
   assert(c1.PI) == 10