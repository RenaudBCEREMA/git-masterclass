# Masterclass TSNI du 21/09/2021

## Programme

1. Git ??
   1. Gestionnaire de version décentralisé
   2. Pour quoi faire
2. Un peu de pratique
   1. Cloner un dépôt
   2. Faire son premier commit
   3. Faire un merge
   4. Gérer les conflits
